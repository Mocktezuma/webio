import React from 'react';

// media imports

function Work() {
    return (
      <section className="work-container">
        <div className="motd">
          <h1>OUR WORK</h1>
          <span className="largeText">Look at what we've made</span>
        </div>
      </section>
    );
  }

  export default Work