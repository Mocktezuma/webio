# Webio coop

WebApp build on React

##### copyright (c) 2019 "all rights reserved"

----

## `Dev Notes`

1) __Recommended:__ Make sure that you have "Live Sass Compiler" settings in "settings.json" have `"liveSassCompile.settings.generateMap": false` and `"liveSassCompile.settings.formats": [{"format": "compressed","extensionName": ".min.css","savePath": null}]` _This is to avoide minor conflicts during development and to optomize development assets while using sass_

----